/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section3_apis.part3_protein_sorting;

import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    private GOannotation goAnnotation;
    private double molecularWeight = -1;

    private static HashMap<String, String> aminoWeights = new HashMap();

    static {
        String aminoAcidWeight = "A 89.095 B 132.1184 C 121.1590 D 133.1032 E 147.1299 F 165.1900 "
                + "G 75.0669 H 155.1552 I 131.1736 K 146.1882 L 131.1736 M 149.2124 N 132.1184 "
                + "P 115.1310 Q 146.1451 R 174.2017 S 105.0930 T 119.1197 V 117.1469 W 204.2262 Y 181.1894";


        //split the string into string array;
        String[] AminoMass = aminoAcidWeight.split(" ");
        //System.out.println(Arrays.toString(AminoMass));

        //create a hashmap;
        //HashMap<String, String> AminoMass_table = new HashMap<String, String>();

        for(int i=0; i<AminoMass.length; i+=2){

            aminoWeights.put(AminoMass[i], AminoMass[i+1]);

        }
        System.out.println(aminoWeights);
    }

    /**
     * constructs without GO annotation;
     * @param name
     * @param accession
     * @param aminoAcidSequence 
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
    }

    /**
     * construicts with main properties.
     * @param name
     * @param accession
     * @param aminoAcidSequence
     * @param goAnnotation 
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        this.goAnnotation = goAnnotation;
    }

    @Override
    public int compareTo(Protein o) {
        int compareName = this.name.compareTo(o.name);

        return compareName;
    }

    public double getMolecularWeight(Protein o) {
        if (molecularWeight == -1) {
        //calculate
            final int BEFORE = -1;
            final int EQUAL = 0;
            final int AFTER = 1;

            String proteinWeight1 = this.getAminoAcidSequence();
            double countProteinWeight1 = 0;

            String proteinWeight2 = o.getAminoAcidSequence();
            double countProteinWeight2 = 0;

            for (char aa: proteinWeight1.toCharArray()){
                countProteinWeight1 +=  Double.parseDouble(aminoWeights.get(String.valueOf(aa)));
            }

            for (char aa: proteinWeight2.toCharArray()){
                countProteinWeight2 += Double.parseDouble(aminoWeights.get(String.valueOf(aa)));
            }

            if (countProteinWeight1 <= countProteinWeight2){return BEFORE;}
            else if (countProteinWeight1 >= countProteinWeight2){return AFTER;}
            else {return EQUAL;}
        }
        return molecularWeight;
    }
    /**
     * provides a range of possible sorters, based on the type that is requested.
     * @param type
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) {
        if (type == null){
            throw new IllegalArgumentException("Error! Type cannot be null!");
        }
        if(type == SortingType.ACCESSION_NUMBER){
            return new AccessionSorter();
        }
        else if (type == SortingType.PROTEIN_WEIGHT){
            return new ProteinWeightSorter();
        }
        return null;
    }

    /**
     *
     * @return name the name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation
     */
    public GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }

}
