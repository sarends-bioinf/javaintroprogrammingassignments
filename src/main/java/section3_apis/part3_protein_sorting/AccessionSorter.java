package section3_apis.part3_protein_sorting;

import section3_apis.part3_protein_sorting.Protein;

import java.util.Comparator;

public class AccessionSorter implements Comparator<Protein> {
    @Override
    public int compare(Protein protein1, Protein protein2) {
        String accession1 = protein1.getAccession().toLowerCase();
        String accession2 = protein2.getAccession().toLowerCase();
        return accession1.compareTo(accession2);
    }
}
