package section3_apis.part1_interfaces;

public class AsciiSumCombiner implements StringCombiner {
    @Override
    public String combine(String first, String second) {
        int sumFirst = first.chars().sum();
        int sumSecond = second.chars().sum();
        return sumFirst + " " + sumSecond;
    }
}
