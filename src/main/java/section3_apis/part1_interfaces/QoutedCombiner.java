package section3_apis.part1_interfaces;

public class QoutedCombiner implements StringCombiner {
    @Override
    public String combine(String first, String second) {
        String result = "'\"" + first  + "\"" + " " + "\"" + second  + "\"'";
        return result;

    }
}
