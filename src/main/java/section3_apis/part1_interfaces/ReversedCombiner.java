package section3_apis.part1_interfaces;

public class ReversedCombiner implements StringCombiner {
    @Override
    public String combine(String first, String second) {
        StringBuilder input1 = new StringBuilder();

        // append a string into StringBuilder input1
        input1.append(first);

        // reverse StringBuilder input1
        input1 = input1.reverse();

        // print reversed String
        //System.out.println(input1);

        StringBuilder input2 = new StringBuilder();

        // append a string into StringBuilder input1
        input2.append(second);

        // reverse StringBuilder input1
        input2 = input2.reverse();

        // print reversed String
        //System.out.println(input2);

        String result = first + input1 + " " + second + input2;
        return result;
    }
}
