package section3_apis.part2_collections;

import java.util.HashMap;
import java.util.Objects;

public class Course {
    private String courseId;
    HashMap<Integer, Double> grades = new HashMap<>();


    public Course(final String courseId) {
        this.courseId = courseId;

    }


    public String getCourseId() {
        return this.courseId;
    }

    public void putGrade(int studId, double courseGrade) {this.grades.put(studId, courseGrade);}

    public  HashMap<Integer, Double> getGrades() {return grades;}
    @Override
    public String toString() {
        return "Course{" +
                "courseId='" + courseId + '\'' +
                "grades=" + grades + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final Course course = (Course) o;
        return this.courseId.equals(course.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.courseId);
    }
}
