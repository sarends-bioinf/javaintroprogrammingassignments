package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    double euclideanDistanceTo(Point otherPoint) {
        //calculate distance - can you implement this?

        //double ac = Math.abs(otherPoint.y - y);
        //double cb = Math.abs(otherPoint.x - x);
        //return Math.hypot(ac, cb);

        double ac = Math.abs(otherPoint.y - y);
        double cb = Math.abs(otherPoint.x - x);
        return Math.sqrt((Math.pow(cb, 2) + Math.pow(ac, 2)));
    }
}
